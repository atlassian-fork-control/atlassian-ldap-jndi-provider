This library can be used instead of the standard LDAP JNDI provider that comes with the JDK - it is a fork with a fix for spurious wakeups affecting the connection timeout.

More information at:

[http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6968459](http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6968459)

[https://bugs.openjdk.java.net/browse/JDK-7011441](https://bugs.openjdk.java.net/browse/JDK-7011441)

------

Use this by setting up JNDI environment with:


```
#!java

env.put(Context.INITIAL_CONTEXT_FACTORY, "com.atlassian.shaded.com.sun.jndi.ldap.LdapCtxFactory");
```

------

Releases are available from [Atlassian 3rd party Maven repository](https://maven.atlassian.com/content/repositories/atlassian-3rdparty/shaded/atlassian/com/sun/jndi/ldap/atlassian-ldap-jndi-provider/).
